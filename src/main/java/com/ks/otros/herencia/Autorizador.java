package com.ks.otros.herencia;

import java.security.PrivateKey;
import java.util.Hashtable;

/**
 * Created by Miguel on 26/08/2016.
 */
public class Autorizador extends Estructura implements procesar
{
    private static Hashtable<String,Autorizador> VMhasDatos;

    public Autorizador()
    {
        super();
    }

    public void guardar()
    {

    }

    public Estructura obtenerDatos(String config)
    {
        return super.buscarHash(config, VMhasDatos);
    }

    public Estructura obtenerDatos(String config, String tienda)
    {
        if (config == null)
        {
            return null;
        }
        else
        {
            return obtenerDatos(config);
        }

    }
}
