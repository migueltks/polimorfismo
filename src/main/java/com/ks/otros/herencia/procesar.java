package com.ks.otros.herencia;

/**
 * Created by Miguel on 26/08/2016.
 */
public interface procesar
{
    public void guardar();
    public Estructura obtenerDatos(String config);
    public Estructura obtenerDatos(String config, String tienda);
}
